# Apiary PoC
#  [https://legalhive.docs.apiary.io/#](https://legalhive.docs.apiary.io/#)


![pepe](images/full_show.gif)
### in this repository we are going to document one of the teams Azure-hosted Lambda functions,
### **fm-lh-ms-courtlistener**


As you can see, changes made to the documentation allow us to ping the _real_ production server and get _real_ answers.

However this comes at a cost: Until we get a valid *token*, all we are going to get will always be a 401 error.

![pepe](images/401_bad_token.png)



Now if instead we write a valid token,
```
78a2e918f3e3bf2c846fa029538b12f5374316ed
```
and a valid judge name
```
Adams, John R.
```
we get the following answer:

![pepe](images/200_good_token.png)
